import 'package:flutter/material.dart';
import 'package:youtube/models/video.dart';

class VideoTile extends StatelessWidget {

  final Video video;

  VideoTile({this.video});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [

          AspectRatio(
            aspectRatio: 16.0/9.0,
            child: Image.network(video.thumb, fit: BoxFit.cover,),
          ),

          SizedBox(height: 8),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [

              Expanded(
                child: Container(
                  margin: EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [

                      Text(
                        video.title,
                        style: TextStyle(fontWeight: FontWeight.w700, color: Colors.white),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      
                      SizedBox(height: 8),
                      Text(
                        video.channel +  ' - 167 mil visualizações - há 1 ano',
                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: Colors.white),
                      ),

                    ],
                  ),
                ),
              ),

              IconButton(
                icon: Icon(Icons.star_border),
                color: Colors.white,
                onPressed: (){},
              ),

            ],
          ),

        ],
      ),
    );
  }
}