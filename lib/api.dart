import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:youtube/models/video.dart';

const API_KEY = 'AIzaSyBdmXr0b1U62IionMaBw7APFt8rNvYwBgw';

class Api {

  String _search, _nextToken;

  Future<List<Video>> search(String search) async {
    this._search = search;

    http.Response response = await http.get("https://www.googleapis.com/youtube/v3/search?part=snippet&q=$search&type=video&key=$API_KEY&maxResults=10");

    return decode(response);

  }

  Future<List<Video>> nextPage() async {
    http.Response response = await http.get("https://www.googleapis.com/youtube/v3/search?part=snippet&q=$_search&type=video&key=$API_KEY&maxResults=10&pageToken=$_nextToken");

    return decode(response);
  }

  List<Video> decode(http.Response response) {

    if (response.statusCode == 200) {

      var decoded = json.decode(response.body);

      this._nextToken = decoded['nextPageToken'];

      List<Video> videos = decoded['items'].map<Video>((map) {
        return Video.fromJson(map);
      }).toList();

      return videos;

    } else {
      throw Exception("Failed to load videos");
    }

  }

}